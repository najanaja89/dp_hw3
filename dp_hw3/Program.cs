﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy
{
    public interface ISubject
    {
        void Request(string pin, string cash);
    }

    //Основной класс, выполняет запрос 
    class Bank : ISubject
    {
        public void Request(string pin, string cash)
        {
            Console.WriteLine($"You took {cash} money");
        }
    }


    //Прокси класс, проверяет пин на корректность
    class ATM : ISubject
    {
        private Bank _bank;

        public ATM(Bank bank)
        {
            _bank = bank;
        }

        public bool CheckAccess(string pin) //Проверка пина
        {
            string _pin = "1234";
            if (_pin == pin)  return true;
            
            else return false;           
        }

        public void Request(string pin, string cash)
        {
            if (this.CheckAccess(pin))
            {
                this._bank = new Bank();
                this._bank.Request(pin, cash);
            }
            else Console.WriteLine("Pin incorrect");
        }
    }

    public class Client
    {
        public void ClientRequest(ISubject subject, string pin, string cash)
        {
            subject.Request(pin, cash);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var pin = "1234";
            var cash = "1500";
            Client client = new Client();
            var ATM = new ATM(new Bank());
            Console.WriteLine("Hello client!");
            Console.WriteLine("Enter pin");
            pin = Console.ReadLine();
            Console.WriteLine("Enter cash value you want got");
            cash = Console.ReadLine();
            client.ClientRequest(ATM, pin, cash);

            Console.ReadLine();
        }
    }
}
