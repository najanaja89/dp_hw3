﻿using Bridge;
using System;

namespace Bridge


    public interface ITheme
    {
        string WhiteTheme();
        string DarkTheme();
        string RedTheme();
    }

    
    //Класс который реализут конкретную тему
    class Theme : ITheme
    {
        public string DarkTheme()
        {
            return "Dark Theme";
        }

        public string RedTheme()
        {
            return "Red Theme";
        }

        public string WhiteTheme()
        {
            return "White Theme";
        }
    }

    //Реализуем абстарктный веб сайта для управления темой сайта
    abstract class WebSite
    {
        protected ITheme _theme;
        public WebSite(ITheme theme)
        {
            _theme = theme;
        }

        public virtual string ChangeThemeDark()
        {
            return "Theme Changed to " + _theme.DarkTheme();
        }

        public virtual string ChangeThemeWhite()
        {
            return "Theme Changed to " + _theme.WhiteTheme();
        }

        public virtual string ChangeThemeRed()
        {
            return "Theme Changed to " + _theme.RedTheme();
        }
    }


    //класс для расширения функционала абстатракции
    class CommonWebSite : WebSite
    {
        public CommonWebSite(ITheme theme) : base(theme)
        {

        }
     }

    class Program
    {
        static void Main(string[] args)
        {
            var themeMenu = "";
            ITheme theme = new Theme();
            WebSite webSite = new CommonWebSite(theme);
            Console.WriteLine("Chose theme of web site 1 - dark, 2 - Red, 3 - White");
            themeMenu = Console.ReadLine();

            switch (themeMenu)
            {
                case "1":
                    Console.WriteLine(webSite.ChangeThemeDark());
                    break;
                case "2":
                    Console.WriteLine(webSite.ChangeThemeRed());
                    break;
                case "3":
                    Console.WriteLine(webSite.ChangeThemeWhite());
                    break;
                default:
                    break;
            }
            Console.ReadLine();

        }
    }

}


//class Client
//{
//    // За исключением этапа инициализации, когда объект Абстракции
//    // связывается с определённым объектом Реализации, клиентский код должен
//    // зависеть только от класса Абстракции. Таким образом, клиентский код
//    // может поддерживать любую комбинацию абстракции и реализации.
//    public void ClientCode(WebSite webSite)
//    {
//        Console.Write(webSite.);
//    }
//}

